import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './component/page-not-found/page-not-found.component';
import { NavComponent } from './component/nav/nav/nav.component';
import { NavButtonComponent } from './component/nav/nav-button/nav-button.component';
import { ArticlesComponent } from './component/articles/articles/articles.component';
import { ArticleComponent } from './component/articles/article/article.component';
import { DetailedArticleComponent } from './component/articles/detailed-article/detailed-article.component';
import { ArticleCreationComponent } from './component/articles/article-creation/article-creation.component';
import {Interceptor} from "./interceptor/interceptor";
import {ArticleService} from "./service/article.service";
import { UsersComponent } from './component/users/users/users.component';
import { UserFormComponent } from './component/users/user-form/user-form.component';
import { UserComponent } from './component/users/user/user.component';
import {CategoryPipe} from "./pipe/category.pipe";
import {TagsPipe} from "./pipe/tags.pipe";
import {TagPipe} from "./pipe/tag.pipe";
import { AnalyticsComponent } from './component/analytics/analytics.component';
import { ContentComponent } from './component/articles/content/content.component';

const appRoutes: Routes = [
  { path: 'users', component: UsersComponent},
  { path: 'users/:login', component: UserComponent},
  { path: 'articles', component: ArticlesComponent },
  { path: 'search/:search', component: ArticlesComponent },
  { path: 'articles/:idArticle', component: DetailedArticleComponent },
  { path: 'articlesCreation', component: ArticleCreationComponent},
  { path: 'analytics', component: AnalyticsComponent},
  { path: '', redirectTo: '/users', pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    NavComponent,
    NavButtonComponent,
    ArticlesComponent,
    ArticleComponent,
    DetailedArticleComponent,
    ArticleCreationComponent,
    UsersComponent,
    UserFormComponent,
    UserComponent,
    CategoryPipe,
    TagsPipe,
    TagPipe,
    AnalyticsComponent,
    ContentComponent
  ],
  imports: [
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes
    ),
    BrowserModule
  ],
  providers: [
    ArticleService,
    { provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
