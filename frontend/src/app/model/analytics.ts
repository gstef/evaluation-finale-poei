export class Analytics {
  articleNumber: number;
  averageTagNumberPerArticle: number;
  averageWordNumberPerArticle: number;
}
