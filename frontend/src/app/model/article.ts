import {Content} from "./content";

export class Article {
  id: string;
  title: string;
  content: Content[];
  synopsis: string;
  author: string;
  status: string;
  category: string;
  tags: string[];

}
