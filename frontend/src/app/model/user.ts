import {Article} from "./article";

export class User {
  id: string;
  username: string;
  email: string;
  password: string;
  articles: Article[];

  constructor(username: string, email: string, password: string) {
    this.username = username;
    this.email = email;
    this.password = password;
  }

}
