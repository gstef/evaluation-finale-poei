import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {Data} from "../model/data";
import {Analytics} from "../model/analytics";

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {

  private PATH = '/analytics';

  constructor(private http: HttpClient) {
  }

  prependUrl(url: string) {
    return this.PATH + url;
  }

  getData(): Observable<Data> {
    const url = this.prependUrl('/data');
    return this.http.get<Data>(url);
  }

  getAnalytics(author: string, category: string, tag: string): Observable<Analytics> {
    const url = this.prependUrl('/stats?' + (author === 'null' ? '' : `author=${author}&`)
      + (category === 'null' ? '' : `category=${category}&`) + (tag === 'null' ? '' : `tag=${tag}`));
    return this.http.get<Analytics>(url);
  }

}
