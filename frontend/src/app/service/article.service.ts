import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Article } from '../model/article';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  private PATH = '/articles';

  constructor(private http: HttpClient) { }

  prependUrl(url: string) {
    return this.PATH + url;
  }

  getAll(search: string = null): Observable<Article[]> {
    let url = this.prependUrl('');
    if (search != null) {
      url = this.prependUrl(`?search=${search}`);
    }
    return this.http.get<Article[]>(url);
  }

  create(article: Article): Observable<Article> {
    const url = this.prependUrl('');
    const body = article;
    return this.http.post<Article>(url, body);
  }

  get(idArticle: string): Observable<Article> {
    const url = this.prependUrl(`/${idArticle}`);
    return this.http.get<Article>(url);
  }

  publish(idArticle: string): Observable<Article> {
    const url = this.prependUrl(`/${idArticle}/:publish`);
    return this.http.post<Article>(url, '');
  }

  modify(idArticle: string, newContent: string): Observable<Article> {
    const url = this.prependUrl(`/${idArticle}`);
    console.log(newContent);
    return this.http.put<Article>(url, newContent);
  }
}
