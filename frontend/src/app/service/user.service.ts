import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../model/user';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private PATH = '/users';

  changeConnection = new EventEmitter<boolean>();

  constructor(
    private http: HttpClient
  ) {
  }

  prependUrl(url: string) {
    return this.PATH + url;
  }

  register(login: string, email: string, password: string): Observable<User> {
    const url = this.prependUrl('/:register');
    const body = new User(login, email, password);
    return this.http.post<User>(url, body);
  }

  login(login: string = null, password: string = null): Observable<User> {
    if (login) {
      this.putIdentifiersInLocalStorage(login, password);
    }
    const url = this.prependUrl(`/${this.getLogin()}`);
    return this.http.get<User>(url);
  }

  isConnected(): boolean {
    return localStorage.getItem('isConnected') === 'true';
  }

  getLogin(): string {
    return localStorage.getItem('login');
  }

  disconnect(): void {
    localStorage.removeItem('isConnected');
    localStorage.removeItem('login');
    localStorage.removeItem('authorization');
    this.changeConnection.emit(false);
  }

  putIdentifiersInLocalStorage(login: string, password: string): void {
    const base64 = btoa(`${login}:${password}`);
    localStorage.setItem('isConnected', 'true');
    localStorage.setItem('login', login);
    localStorage.setItem('authorization', `Basic ${base64}`);
  }

}

