import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'displayTags'})
export class TagsPipe implements PipeTransform {
  transform(tags: string[]): string {
    return tags.map(s => this.presentToDisplay(s)).join(", ")
  }

  presentToDisplay(s: string): string {
    return s.charAt(0).toUpperCase() + s.substr(1).toLowerCase();
  }

}
