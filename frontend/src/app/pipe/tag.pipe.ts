import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'displayTag'})
export class TagPipe implements PipeTransform {
  transform(tag: string): string {
    return tag.charAt(0).toUpperCase() + tag.substr(1).toLowerCase();
  }

}
