import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'display'})
export class CategoryPipe implements PipeTransform {
  transform(category: string): string {
    switch (category) {
      case 'METEO': return 'Météo';
      case 'HOBBIES': return 'Loisirs';
      case 'POLITICS': return 'Politique';
      case 'SPORTS': return 'Sports';
    }
  }
}
