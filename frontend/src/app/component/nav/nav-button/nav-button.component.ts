import {Component, Input, OnInit} from '@angular/core';
import {UserService} from "../../../service/user.service";

@Component({
  selector: 'app-nav-button',
  templateUrl: './nav-button.component.html',
  styleUrls: ['./nav-button.component.css']
})
export class NavButtonComponent implements OnInit {
  @Input() text: string;
  @Input() link: string;

  connected: boolean;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.connected = this.userService.isConnected();
    if (!this.text) {
      this.text = this.connected ? 'Déconnexion' : 'Connexion';
    }
    this.userService.changeConnection.subscribe((b: boolean) => {
      this.connected = b;
      if (this.connected && this.text === 'Connexion') {
        this.text = 'Déconnexion';
      } else if (!this.connected && this.text === 'Déconnexion') {
        this.text = 'Connexion';
      }
    });
  }

  onClickCheckAndDisconnect() {
    if (this.text === 'Déconnexion') {
      this.userService.disconnect();
    }
  }

}
