import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../service/user.service";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  loginLink: string;
  isConnected: boolean;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.loginLink = `/users/${this.userService.getLogin() ? this.userService.getLogin() : ''}`;
    this.isConnected = this.userService.isConnected();
    this.userService.changeConnection.subscribe(
      (b: boolean) => {
        this.loginLink = b ? `/users/${this.userService.getLogin()}` : '/users';
        this.isConnected = b;
      }
    );
  }

}
