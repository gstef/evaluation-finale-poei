import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserService} from "../../../service/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  @Input() reset: boolean;
  @Input() role: string;
  login: string;
  password: string;
  email: string;
  @Output() modified = new EventEmitter();
  textH2: string;
  incorrectIdentifiers = false;

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit() {
    if (this.role === 'login') {
      this.textH2 = 'Vous avez déjà un compte?';
    } else if (this.role === 'register') {
      this.textH2 = 'Créez un nouveau compte.';
    } else {
      this.textH2 = 'Changez vos identifiants.';
    }
  }

  ngOnChanges() {
    if (this.reset) {
      this.reset = false;
      this.incorrectIdentifiers = false;
      this.login = '';
      this.password = '';
    }
  }

  checkStringForColonAndSlash(s: string) {
    return s.includes(':') || s.includes('/');
  }

  onChangeCheck() {
    this.modified.emit();
    this.incorrectIdentifiers = (this.login && this.checkStringForColonAndSlash(this.login))
      || (this.password && this.checkStringForColonAndSlash(this.password));
  }

  connect() {
    this.userService.putIdentifiersInLocalStorage(this.login, this.password);
    this.userService.changeConnection.emit(true);
    this.router.navigate([`/users/${this.login}`]);
  }

  disconnect() {
    this.incorrectIdentifiers = true;
    this.userService.disconnect();
  }

  onClickValidate() {
    this.incorrectIdentifiers = false;
    if (this.role === 'login') {
      this.userService.login(this.login, this.password).subscribe(
        () => this.connect(),
        () => this.disconnect()
      );
    } else if (this.role === 'register') {
      this.userService.register(this.login, this.email, this.password).subscribe(
        () => this.connect(),
        () => this.disconnect()
      );
    }
  }
}
