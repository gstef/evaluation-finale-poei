import { Component, OnInit } from '@angular/core';
import {User} from "../../../model/user";
import {UserService} from "../../../service/user.service";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  login: string;
  user: User;

  constructor(
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    if (!this.userService.isConnected()) {
      this.router.navigate(['/users']);
    } else {
      this.route.paramMap.subscribe((params: ParamMap) => {
        this.login = params.get('login');
        this.userService.login().subscribe(user => {
          this.user = user;
          if (this.login !== this.user.username) {
            this.userService.disconnect();
            this.router.navigate(['/users']);
          }
        });
      });
    }
  }

}
