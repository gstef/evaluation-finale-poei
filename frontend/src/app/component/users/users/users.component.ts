import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../service/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  connected: boolean;
  resetR: boolean;
  resetL: boolean;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.connected = this.userService.isConnected();
    if (this.connected) {
      this.router.navigate([`/users/${this.userService.getLogin()}`]);
    }
    this.userService.changeConnection.subscribe((b: boolean) => {
      this.connected = b;
      if (this.connected) {
        this.router.navigate([`/users/${this.userService.getLogin()}`]);
      }
    });
  }

  onModificationResetRegister() {
    this.resetR = true;
    this.resetL = false;
  }

  onModificationResetLogin() {
    this.resetL = true;
    this.resetR = false;
  }

}
