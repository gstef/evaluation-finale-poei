import {Component, OnInit} from '@angular/core';
import {ArticleService} from "../../../service/article.service";
import {Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import {UserService} from "../../../service/user.service";

@Component({
  selector: 'app-article-creation',
  templateUrl: './article-creation.component.html',
  styleUrls: ['./article-creation.component.css']
})
export class ArticleCreationComponent implements OnInit {
  categories = ['METEO', 'POLITICS', 'SPORTS', 'HOBBIES'];
  selectedCategory = 'METEO';
  tags: string[] = [];
  tag: string;
  invalidArguments = false;

  constructor(private articleService: ArticleService,
              private userService: UserService,
              private router: Router) {
  }


  ngOnInit() {
    if (!this.userService.isConnected()) {
      this.router.navigate(['/users']);
    }
  }

  onClickSend(form: NgForm) {
    let tempForm = form.value;
    if (tempForm.title === '' || tempForm.synopsis === '') {
      this.invalidArguments = true;
    } else {
      tempForm.tags = this.tags;
      this.articleService.create(tempForm).subscribe(a => this.router.navigate([`/articles/${a.id}`]));
    }
  }

  onClickAddTag() {
    this.tags.push(this.tag);
    this.tags = [].concat(this.tags);
  }

  onChangeReset() {
    this.invalidArguments = false;
  }
}
