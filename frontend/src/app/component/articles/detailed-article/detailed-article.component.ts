import {Component, OnInit} from '@angular/core';
import {Article} from "../../../model/article";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";
import {ArticleService} from "../../../service/article.service";

@Component({
  selector: 'app-detailed-article',
  templateUrl: './detailed-article.component.html',
  styleUrls: ['./detailed-article.component.css']
})
export class DetailedArticleComponent implements OnInit {
  article: Article;
  idArticle: string;
  modify: string;
  newContent: string;

  constructor(private route: ActivatedRoute,
              private articleService: ArticleService,
              private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.idArticle = params.get('idArticle');
      this.modify = params.get('modify');
      this.articleService.get(this.idArticle).subscribe(article => {
        this.article = article;
        this.newContent = article.content[0].content;
      });
    })
  }

  onClickModify() {
    this.articleService.modify(this.idArticle, this.newContent).subscribe(() =>
    this.router.navigate([`/articles/${this.idArticle}`]));
  }
}
