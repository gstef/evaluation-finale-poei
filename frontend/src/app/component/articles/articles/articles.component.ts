import {Component, OnInit} from '@angular/core';
import {ArticleService} from "../../../service/article.service";
import {Article} from "../../../model/article";
import {ActivatedRoute, ParamMap, Router} from "@angular/router";

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {
  articles: Article[];
  filteredArticles: Article[];
  authors: string[] = [];
  selectedAuthor = 'null';
  search: string;
  categories = ['METEO', 'POLITICS', 'SPORTS', 'HOBBIES'];
  tags: string[] = [];
  selectedCategory = 'null';
  selectedTag = 'null';

  constructor(private articleService: ArticleService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      if (params.get('search') == null) {
        this.articleService.getAll().subscribe(articles => {
          this.initializeArticles(articles);
        });
      } else {
        this.articleService.getAll(params.get('search')).subscribe(articles => {
          this.initializeArticles(articles);
        });
      }
    })
  }

  private initializeArticles(articles: Article[]) {
    this.articles = articles;
    this.filteredArticles = articles;
    this.articles.forEach(a => {
      if (!this.authors.includes(a.author)) {
        this.authors.push(a.author);
      }
      a.tags.forEach(t => {
        if (!this.tags.includes(t)) {
          this.tags.push(t);
        }
      })
    });
  }

  onClickFilter() {
    this.filteredArticles = this.articles;
    if (this.selectedAuthor !== 'null'){
      this.filteredArticles = this.filteredArticles.filter(a => a.author === this.selectedAuthor);
    }
    if (this.selectedCategory !== 'null'){
      this.filteredArticles = this.filteredArticles.filter(a => a.category === this.selectedCategory);
    }
    if (this.selectedTag !== 'null'){
      this.filteredArticles = this.filteredArticles.filter(a => a.tags.includes(this.selectedTag));
    }
  }

  onClickSearch() {
    this.router.navigate([`/search/${this.search}`])
  }

}
