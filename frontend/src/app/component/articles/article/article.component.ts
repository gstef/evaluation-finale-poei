import {Component, Input, OnInit} from '@angular/core';
import {Article} from "../../../model/article";
import {Router} from "@angular/router";
import {ArticleService} from "../../../service/article.service";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  @Input() article: Article;
  @Input() displayUser = false;

  constructor(private articleService: ArticleService,
    private router: Router) { }

  ngOnInit() {
  }

  onClickGoToSee() {
    this.router.navigate([`/articles/${this.article.id}`]);
  }

  onClickPublish() {
    this.articleService.publish(this.article.id).subscribe(article => this.article = article);
  }

  onClickGoToModify() {
    this.router.navigate([`/articles/${this.article.id}`, {modify: true}]);
  }
}
