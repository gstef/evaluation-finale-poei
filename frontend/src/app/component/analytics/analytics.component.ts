import { Component, OnInit } from '@angular/core';
import {AnalyticsService} from "../../service/analytics.service";
import {Data} from "../../model/data";
import {Analytics} from "../../model/analytics";

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.css']
})
export class AnalyticsComponent implements OnInit {
  selectedAuthor: string = 'null';
  data: Data;
  selectedCategory: string = 'null';
  selectedTag: string = 'null';
  stats: Analytics;

  constructor(private analyticsService: AnalyticsService) { }

  ngOnInit() {
    this.analyticsService.getData().subscribe(data => this.data = data);
  }

  onClickFilter() {
    this.analyticsService.getAnalytics(this.selectedAuthor, this.selectedCategory, this.selectedTag)
      .subscribe(stats => this.stats = stats);
  }
}
