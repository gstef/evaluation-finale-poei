package com.zenika.academy.ZenikaBlog.persistence.model.article;

import com.zenika.academy.ZenikaBlog.persistence.model.user.User;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringJUnitConfig
class ArticleTest {

    User user = new User(UUID.randomUUID(), "user", "email@email.com", "password");


    @Test
    void constructorWithValidArguments() {
        assertDoesNotThrow(() -> new Article(UUID.randomUUID(), "test", "test", "test", this.user,
                Category.METEO, List.of()));
    }

    @Test
    void constructorWithSynopsisTooLong() {
        assertThrows(IllegalArgumentException.class, () -> new Article(UUID.randomUUID(), "test", "test",
                "test test test test test test test test test test test test", this.user, Category.HOBBIES,
                List.of()));

    }

    @Test
    void constructorWithNullCategory() {
        assertThrows(IllegalArgumentException.class, () -> new Article(UUID.randomUUID(), "test", "test",
                "test", this.user, null, List.of()));

    }
}