package com.zenika.academy.ZenikaBlog.persistence.model.analytics;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import static org.junit.jupiter.api.Assertions.*;

@SpringJUnitConfig
class AnalyticsTest {

    @Test
    void count() {
        assertEquals(2, Analytics.count("Le soleil."));
    }
}