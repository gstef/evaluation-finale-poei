CREATE TABLE articles
(
    id        UUID PRIMARY KEY,
    title     TEXT NOT NULL,
    synopsis  VARCHAR(50) NOT NULL,
    content   TEXT
)