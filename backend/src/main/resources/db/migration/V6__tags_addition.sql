ALTER TABLE articles ADD COLUMN tags TEXT NOT NULL DEFAULT '';

UPDATE articles SET tags = 'SOLEIL, TEMPS' WHERE id = '61d1db68-7098-4cf2-9b25-a6d88af17e1c';

INSERT INTO  articles VALUES ('821a9ab7-825d-4903-9628-1a81b5b16500', 'Se bronzer au soleil', 'Le sable blanc!',
                              'Pas de nuages que du soleil', '5091b5fc-0afa-47bf-9178-7e5064d823c6', 'PUBLISHED',
                              'METEO', 'SOLEIL, SE BRONZER, BIEN-ÊTRE');

INSERT INTO  articles VALUES ('1014def6-3460-4a7a-84de-3272196215c9', 'Le président', 'Qu''il est intelligent !',
                              'Quel beau discours', '5091b5fc-0afa-47bf-9178-7e5064d823c6', 'PUBLISHED',
                              'POLITICS', 'POLITIQUE, DISCOURS, PRÉSIDENT');

INSERT INTO  articles VALUES ('936114da-1709-492a-a446-d915a8bdf5b2', 'La natation', 'Ça fait du bien de nager!',
                              'A la piscine ou à la mer, il faut nager!', 'd095e7dd-f5c1-49da-8cf3-8ec8af03edf5',
                              'PUBLISHED', 'SPORTS', 'NATATION, SPORT, BIEN-ÊTRE');

INSERT INTO  articles VALUES ('32053be1-c846-43bb-8ab6-8fcd8b3b3d2c', 'Les jeux de stratégie', 'Jouer, ça fait réfléchir',
                              'Jeu de plateau, jeu de cartes, ou jeu vidéo, toute excuse est la bonne',
                              'd095e7dd-f5c1-49da-8cf3-8ec8af03edf5', 'PUBLISHED',
                              'HOBBIES', 'LOISIR, JEUX, STRATÉGIE, BIEN-ÊTRE');