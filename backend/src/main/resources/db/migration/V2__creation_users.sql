CREATE TABLE users
(
    id        UUID PRIMARY KEY,
    username  VARCHAR(50) NOT NULL UNIQUE,
    email     VARCHAR(50),
    password  VARCHAR(60) NOT NULL
);

INSERT INTO users VALUES('5091b5fc-0afa-47bf-9178-7e5064d823c6', 'anonymous', 'anonymous@anonymous.com',
                         '$2a$10$mOpRhe94nUo6XAHYEEwK6eEK7TZKBbCHK3WoqK/IxpcJl4B7xl792');

ALTER TABLE articles ADD COLUMN id_user UUID REFERENCES users (id) NOT NULL default '5091b5fc-0afa-47bf-9178-7e5064d823c6';

INSERT INTO users VALUES('d095e7dd-f5c1-49da-8cf3-8ec8af03edf5', 'user', 'user@email.com',
                         '$2a$10$mOpRhe94nUo6XAHYEEwK6eEK7TZKBbCHK3WoqK/IxpcJl4B7xl792');