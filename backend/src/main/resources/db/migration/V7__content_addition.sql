CREATE TABLE content
(
    id          SERIAL PRIMARY KEY,
    type        VARCHAR(5) NOT NULL DEFAULT 'TEXT',
    content     TEXT,
    id_article  UUID REFERENCES articles (id)
);

INSERT INTO content (content, id_article) SELECT content, id  FROM articles;

ALTER TABLE articles DROP COLUMN content;

INSERT INTO content (type, content, id_article)
VALUES ('IMAGE', 'https://img-31.ccm2.net/JgViH6yxAeWw8arbb4P5EwjFXzw=/1240x/smart/b490b7fb4be14ba2a1f8ff0c1a2a9267/' ||
                 'ccmcms-hugo/10608091.jpg','61d1db68-7098-4cf2-9b25-a6d88af17e1c')