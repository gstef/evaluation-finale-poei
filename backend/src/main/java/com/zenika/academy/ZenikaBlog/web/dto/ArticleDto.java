package com.zenika.academy.ZenikaBlog.web.dto;

import com.zenika.academy.ZenikaBlog.persistence.model.article.Article;
import com.zenika.academy.ZenikaBlog.persistence.model.article.Category;
import com.zenika.academy.ZenikaBlog.persistence.model.article.Status;
import lombok.Value;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Value
public class ArticleDto {
    UUID id;
    String title;
    List<ContentDto> content;
    String synopsis;
    String author;
    Status status;
    Category category;
    List<String> tags;

    public static ArticleDto mapper(Article article) {
        return new ArticleDto(article.getId(), article.getTitle(),
                article.getContent().stream().map(ContentDto::mapper).collect(Collectors.toList()),
                article.getSynopsis(), article.getUser().getUsername(), article.getStatus(), article.getCategory(),
                article.getTags());
    }
}
