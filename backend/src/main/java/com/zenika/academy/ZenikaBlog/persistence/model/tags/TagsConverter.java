package com.zenika.academy.ZenikaBlog.persistence.model.tags;

import javax.persistence.AttributeConverter;
import java.util.ArrayList;
import java.util.Arrays;

public class TagsConverter implements AttributeConverter<Tags, String> {
    @Override
    public String convertToDatabaseColumn(Tags tags) {
        return String.join(", ", tags.get());
    }

    @Override
    public Tags convertToEntityAttribute(String dbData) {
        if (dbData.isEmpty()) {
            return new Tags(new ArrayList<>());
        } else {
            return new Tags(Arrays.asList(dbData.split(", ")));
        }
    }
}
