package com.zenika.academy.ZenikaBlog.service;

import com.zenika.academy.ZenikaBlog.persistence.dao.ArticleRepository;
import com.zenika.academy.ZenikaBlog.persistence.model.article.Article;
import com.zenika.academy.ZenikaBlog.persistence.model.user.User;
import com.zenika.academy.ZenikaBlog.util.IdGenerator;
import com.zenika.academy.ZenikaBlog.web.dto.ArticleDto;
import com.zenika.academy.ZenikaBlog.web.dto.UserArticleDto;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class ArticleService {
    private Logger logger;
    private ArticleRepository articleRepository;
    private UserService userService;
    private IdGenerator idGenerator;

    @Autowired
    public ArticleService(Logger logger, ArticleRepository articleRepository,
                          UserService userService, IdGenerator idGenerator) {
        this.logger = logger;
        this.articleRepository = articleRepository;
        this.userService = userService;
        this.idGenerator = idGenerator;
    }

    public ArticleDto create(UserArticleDto article) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = ((UserDetails) authentication.getPrincipal()).getUsername();
        User currentUser = this.userService.findByUsername(username);
        Article newArticle = new Article(this.idGenerator.generateNewId(), article.getTitle(), article.getContent(),
                article.getSynopsis(), currentUser, article.getCategory(), article.getTags());
        currentUser.getArticles().add(newArticle);
        this.logger.info(String.format("Created a new article with id = %s and title = %s", newArticle.getId(),
                newArticle.getTitle()));
        return ArticleDto.mapper(this.articleRepository.save(newArticle));
    }

    public List<ArticleDto> getAll() {
        return this.articleRepository.findPublishedArticles()
                .stream()
                .map(ArticleDto::mapper)
                .collect(Collectors.toList());
    }

    Optional<Article> get(UUID id) {
        return this.articleRepository.findById(id);
    }

    public Optional<ArticleDto> getById(UUID id) {
        return this.get(id).map(ArticleDto::mapper);
    }

    public Optional<ArticleDto> modify(UUID id, String newContent) {
        Optional<Article> oArticle = this.get(id);
        return oArticle.map(a -> {
            if (this.userService.getCurrentUsername().equals(a.getUser().getUsername())) {
                a.setContent(newContent);
                return ArticleDto.mapper(this.articleRepository.save(a));
            } else {
                this.logger.warn("Cannot modify the article of another user.");
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Cannot modify the article of another user.");
            }
        });
    }

    public Optional<ArticleDto> publish(UUID id) {
        Optional<Article> oArticle = this.get(id);
        return oArticle.map(a -> {
            if (this.userService.getCurrentUsername().equals(a.getUser().getUsername())) {
                a.publish();
                return ArticleDto.mapper(this.articleRepository.save(a));
            } else {
                this.logger.warn("Cannot publish the article of another user.");
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Cannot publish the article of another user.");
            }
        });
    }

    public List<ArticleDto> searchTitles(String search) {
        return this.articleRepository.searchTitles(search)
                .stream()
                .map(ArticleDto::mapper)
                .collect(Collectors.toList());
    }

}
