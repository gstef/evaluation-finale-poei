package com.zenika.academy.ZenikaBlog.web.dto;

import com.zenika.academy.ZenikaBlog.persistence.model.user.User;
import lombok.Value;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Value
public class UserDto {
    UUID id;
    String username;
    String email;
    String password;
    List<ArticleDto> articles;

    public static UserDto mapper(User user) {
        return new UserDto(user.getId(), user.getUsername(), user.getEmail(), null,
                user.getArticles()
                        .stream()
                        .map(ArticleDto::mapper)
                        .collect(Collectors.toList()));
    }
}
