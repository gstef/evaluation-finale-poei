package com.zenika.academy.ZenikaBlog.util;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class IdGenerator {
    public UUID generateNewId(){
        return UUID.randomUUID();
    }
}
