package com.zenika.academy.ZenikaBlog.persistence.model.article;

public enum Category {
    METEO,
    SPORTS,
    POLITICS,
    HOBBIES
}
