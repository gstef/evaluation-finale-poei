package com.zenika.academy.ZenikaBlog.persistence.model.analytics;

import com.zenika.academy.ZenikaBlog.persistence.model.article.Article;
import com.zenika.academy.ZenikaBlog.persistence.model.content.Content;
import com.zenika.academy.ZenikaBlog.persistence.model.content.ContentType;

import java.util.Collection;
import java.util.List;

public class Analytics {

    private final int articleNumber;
    private final float averageTagNumberPerArticle;
    private final float averageWordNumberPerArticle;


    public Analytics(List<Article> articles) {
        this.articleNumber = articles.size();
        this.averageTagNumberPerArticle = articles.stream()
                .map(Article::getTags)
                .map(List::size)
                //.mapToInt(Integer::intValue).summaryStatistics()
                .reduce(Integer::sum)
                .orElse(0) / (float) this.articleNumber;
        this.averageWordNumberPerArticle = articles.stream()
                .map(Article::getContent)
                .flatMap(Collection::stream)
                .filter(c -> c.getType().equals(ContentType.TEXT))
                .map(Content::getContent)
                .map(Analytics::count)
                .reduce(Integer::sum)
                .orElse(0) / (float) this.articleNumber;
    }

    public int getArticleNumber() {
        return this.articleNumber;
    }

    public float getAverageTagNumberPerArticle() {
        return this.averageTagNumberPerArticle;
    }

    public float getAverageWordNumberPerArticle() {
        return this.averageWordNumberPerArticle;
    }

    public static int count(String sentence){
        if(sentence == null || sentence.isEmpty()){
            return 0;
        }

        String[] words = sentence.split("\\s+");
        return words.length;
    }

}
