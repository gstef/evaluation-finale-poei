package com.zenika.academy.ZenikaBlog.persistence.model.tags;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Tags {
    private final Set<String> tags;

    public Tags(List<String> tags) {
        this.tags = new HashSet<>();
        tags.stream().map(String::toUpperCase).forEach(this.tags::add);

    }

    public Set<String> get() {
        return this.tags;
    }
}
