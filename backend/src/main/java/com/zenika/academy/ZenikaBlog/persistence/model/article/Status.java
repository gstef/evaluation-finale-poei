package com.zenika.academy.ZenikaBlog.persistence.model.article;

public enum Status {
    DRAFT,
    PUBLISHED
}
