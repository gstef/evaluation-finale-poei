package com.zenika.academy.ZenikaBlog.persistence.dao;

import com.zenika.academy.ZenikaBlog.persistence.model.article.Article;
import com.zenika.academy.ZenikaBlog.persistence.model.tags.Tags;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.UUID;

public interface ArticleRepository extends CrudRepository<Article, UUID> {
    List<Article> findAll();

    @Query("SELECT a FROM Article a WHERE a.status = 'PUBLISHED'")
    List<Article> findPublishedArticles();

    @Query("SELECT a FROM Article a WHERE a.title LIKE function('concat','%', ?1, '%') OR " +
            "function('levenshtein', a.title, ?1) <= 4 AND a.status = 'PUBLISHED'")
    List<Article> searchTitles(String search);

    @Query("SELECT a.tags FROM Article a")
    List<Tags> getTags();

    @Query("SELECT DISTINCT a.user.username FROM Article a")
    List<String> getAuthors();
}
