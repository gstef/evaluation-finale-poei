package com.zenika.academy.ZenikaBlog.persistence.model.article;

import com.zenika.academy.ZenikaBlog.persistence.model.content.Content;
import com.zenika.academy.ZenikaBlog.persistence.model.content.ContentType;
import com.zenika.academy.ZenikaBlog.persistence.model.tags.Tags;
import com.zenika.academy.ZenikaBlog.persistence.model.tags.TagsConverter;
import com.zenika.academy.ZenikaBlog.persistence.model.user.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "articles")
public class Article {

    @Id
    private UUID id;

    private String title;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "id_article")
    private List<Content> content;

    private String synopsis;

    @Enumerated(value = EnumType.STRING)
    private Status status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user", referencedColumnName = "id")
    private User user;

    @Enumerated(value = EnumType.STRING)
    private Category category;

    @Convert(converter = TagsConverter.class)
    private Tags tags;

    @Deprecated
    public Article() {
    }

    public Article(UUID id, String title, String content, String synopsis, User user, Category category,
                   List<String> tags) {
        if (id == null) {
            throw new IllegalArgumentException("L'id ne peut pas être null");
        }

        if (title == null || title.isEmpty() || synopsis == null || synopsis.isEmpty()) {
            throw new IllegalArgumentException("Titre et synopsis ne peuvent pas être vide");
        }

        if (category == null) {
            throw new IllegalArgumentException("La catégorie ne peut pas être nulle");
        }
        if (synopsis.length() > 50) {
            throw new IllegalArgumentException("Le résumé ne doit pas comporter plus de 50 caractères");
        }

        if (user == null) {
            throw new IllegalArgumentException("L'utilisateur ne peut pas être null");
        }

        this.id = id;
        this.title = title;
        this.content = new ArrayList<>();
        this.setContent(content);
        this.synopsis = synopsis;
        this.status = Status.DRAFT;
        this.user = user;
        this.category = category;
        this.tags = new Tags(tags == null ? new ArrayList<>() : tags);
    }

    public UUID getId() {
        return this.id;
    }

    public String getTitle() {
        return this.title;
    }

    public List<Content> getContent() {
        return this.content;
    }

    public String getSynopsis() {
        return this.synopsis;
    }

    public User getUser() {
        return this.user;
    }

    public Status getStatus() {
        return this.status;
    }

    public Category getCategory() {
        return this.category;
    }

    public List<String> getTags() {
        return new ArrayList<>(this.tags.get());
    }

    public void setContent(String content) {
        this.content.clear();
        this.content.add(new Content(ContentType.TEXT, content));
        //this.content = List.of(new Content(ContentType.TEXT, content));
    }

    public void publish() {
        this.status = Status.PUBLISHED;
    }
}
