package com.zenika.academy.ZenikaBlog.service;

import com.zenika.academy.ZenikaBlog.persistence.dao.UserRepository;
import com.zenika.academy.ZenikaBlog.persistence.model.user.User;
import com.zenika.academy.ZenikaBlog.util.IdGenerator;
import com.zenika.academy.ZenikaBlog.web.dto.UserDto;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Component
public class UserService {
    private Logger logger;
    private IdGenerator idGenerator;
    private UserRepository userRepository;
    private PasswordEncoder encoder;

    @Autowired
    public UserService(Logger logger, IdGenerator idGenerator, UserRepository userRepository, PasswordEncoder encoder) {
        this.logger = logger;
        this.idGenerator = idGenerator;
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    public UserDto register(UserDto user) {
        if (this.userRepository.existsByUsername(user.getUsername())) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, String.format("Username %s already in use",
                    user.getUsername()));
        }

        User newUser = new User(this.idGenerator.generateNewId(), user.getUsername(), user.getEmail(),
                this.encoder.encode(user.getPassword()));

        this.logger.info(String.format("Created a user with id = %s and username = %s", newUser.getId(),
                newUser.getUsername()));
        return UserDto.mapper(this.userRepository.save(newUser));
    }

    public User findByUsername(String username) {
        return this.userRepository.findByUsername(username)
                .orElseThrow(() -> {
                    this.logger.warn(String.format("Username %s does not exist.", username));
                    throw new UsernameNotFoundException(String.format("Username %s does not exist.", username));
                });

    }

    public UserDto login(String username) {
        String authUsername = this.getCurrentUsername();

        if (!authUsername.equals(username)) {
            throw new IllegalArgumentException("Username does not match authentication");
        }

        return UserDto.mapper(this.findByUsername(username));
    }

    String getCurrentUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ((UserDetails) authentication.getPrincipal()).getUsername();
    }

}
