package com.zenika.academy.ZenikaBlog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZenikaBlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZenikaBlogApplication.class, args);
	}

}
