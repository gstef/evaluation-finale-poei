package com.zenika.academy.ZenikaBlog.web.controller;

import com.zenika.academy.ZenikaBlog.persistence.model.article.Category;
import com.zenika.academy.ZenikaBlog.service.AnalyticsService;
import com.zenika.academy.ZenikaBlog.web.dto.DataDto;
import com.zenika.academy.ZenikaBlog.web.dto.AnalyticsDto;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/analytics")
public class AnalyticsController {
    private Logger logger;
    private AnalyticsService analyticsService;

    @Autowired
    public AnalyticsController(Logger logger, AnalyticsService analyticsService) {
        this.logger = logger;
        this.analyticsService = analyticsService;
    }

    @GetMapping("/data")
    public DataDto getData() {
        this.logger.info("Received a GET request on /analytics/data");
        return this.analyticsService.getData();
    }

    @GetMapping("/stats")
    public AnalyticsDto getAnalytics(@RequestParam(name = "author", required = false) String author,
                                     @RequestParam(name = "category", required = false) Category category,
                                     @RequestParam(name = "tag", required = false) String tag) {
        this.logger.info("Received a GET request on /analytics/stats");
        return this.analyticsService.getAnalytics(author, category, tag);
    }

}
