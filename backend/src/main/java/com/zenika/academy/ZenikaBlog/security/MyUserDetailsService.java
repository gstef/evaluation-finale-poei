package com.zenika.academy.ZenikaBlog.security;

import com.zenika.academy.ZenikaBlog.persistence.model.user.User;
import com.zenika.academy.ZenikaBlog.service.UserService;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyUserDetailsService implements UserDetailsService {
    private Logger logger;
    private UserService userService;

    @Autowired
    public MyUserDetailsService(Logger logger, UserService userService) {
        this.logger = logger;
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.userService.findByUsername(username);
        this.logger.info(String.format("Loaded user at username = %s", username));
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                List.of());
    }


}
