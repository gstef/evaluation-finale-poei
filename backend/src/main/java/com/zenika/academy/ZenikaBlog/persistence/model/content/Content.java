package com.zenika.academy.ZenikaBlog.persistence.model.content;

import javax.persistence.*;

@Entity
public class Content {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    private ContentType type;

    private String content;

    @Deprecated
    public Content() {
    }

    public Content(ContentType type, String content) {
        this.type = type;
        this.content = content;
    }

    public ContentType getType() {
        return this.type;
    }

    public String getContent() {
        return this.content;
    }

}
