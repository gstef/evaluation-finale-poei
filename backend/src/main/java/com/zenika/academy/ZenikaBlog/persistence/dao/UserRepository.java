package com.zenika.academy.ZenikaBlog.persistence.dao;

import com.zenika.academy.ZenikaBlog.persistence.model.user.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends CrudRepository<User, UUID> {
     List<User> findAll();

     Optional<User> findByUsername(String username);

     boolean existsByUsername(String username);
}
