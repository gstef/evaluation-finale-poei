package com.zenika.academy.ZenikaBlog.web.dto;

import com.zenika.academy.ZenikaBlog.persistence.model.content.Content;
import com.zenika.academy.ZenikaBlog.persistence.model.content.ContentType;
import lombok.Value;

@Value
public class ContentDto {
    ContentType type;
    String content;

    public static ContentDto mapper(Content content) {
        return new ContentDto(content.getType(), content.getContent());
    }
}
