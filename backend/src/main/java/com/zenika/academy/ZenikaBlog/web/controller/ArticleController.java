package com.zenika.academy.ZenikaBlog.web.controller;

import com.zenika.academy.ZenikaBlog.service.ArticleService;
import com.zenika.academy.ZenikaBlog.web.dto.ArticleDto;
import com.zenika.academy.ZenikaBlog.web.dto.UserArticleDto;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/articles")
public class ArticleController {
    private Logger logger;
    private ArticleService service;

    @Autowired
    public ArticleController(Logger logger, ArticleService service) {
        this.logger = logger;
        this.service = service;
    }

    @GetMapping("")
    public List<ArticleDto> getAll(@RequestParam(name = "search", required = false) String search) {
        if (search == null) {
            this.logger.info("Received a GET request on /articles");
            return this.service.getAll();
        } else {
            this.logger.info("Received a GET request on /articles?search=" + search);
            return this.service.searchTitles(search);
        }


    }

    @GetMapping("/{id}")
    public ArticleDto getById(@PathVariable(name = "id") UUID id) {
        this.logger.info(String.format("Received a GET request on /articles/%s", id));
        return this.service.getById(id).orElseThrow(() -> {
            this.logger.warn(String.format("Article not found at id = %s", id));
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Article not found at id = %s", id));
        });
    }

    @PostMapping("")
    public ArticleDto create(@RequestBody UserArticleDto article) {
        this.logger.info("Received a POST request on /articles");
        try {
            return this.service.create(article);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid arguments", e);
        }

    }

    @PutMapping("/{id}")
    public ArticleDto modify(@PathVariable(name = "id") UUID id, @RequestBody String newContent) {
        this.logger.info("Received a PUT request on /articles/" + id);
        return this.service.modify(id, newContent).orElseThrow(() -> {
            this.logger.warn("No article found at id = " + id);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No article found at id = " + id);
        });
    }

    @PostMapping("/{id}/:publish")
    public ArticleDto publish(@PathVariable(name = "id") UUID id) {
        this.logger.info(String.format("Received a POST request on /articles/%s/:publish", id));
        return this.service.publish(id).orElseThrow(() -> {
            this.logger.warn("No article found at id = " + id);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No article found at id = " + id);
        });
    }

}
