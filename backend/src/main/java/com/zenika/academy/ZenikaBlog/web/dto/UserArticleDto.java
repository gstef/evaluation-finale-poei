package com.zenika.academy.ZenikaBlog.web.dto;

import com.zenika.academy.ZenikaBlog.persistence.model.article.Category;
import lombok.Value;

import java.util.List;
import java.util.UUID;

@Value
public class UserArticleDto {
    UUID id;
    String title;
    String content;
    String synopsis;
    String author;
    Category category;
    List<String> tags;
}

