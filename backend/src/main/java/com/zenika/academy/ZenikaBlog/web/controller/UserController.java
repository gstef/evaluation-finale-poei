package com.zenika.academy.ZenikaBlog.web.controller;

import com.zenika.academy.ZenikaBlog.service.UserService;
import com.zenika.academy.ZenikaBlog.web.dto.UserDto;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/users")
public class UserController {
    private Logger logger;
    private UserService service;

    @Autowired
    public UserController(Logger logger, UserService service) {
        this.logger = logger;
        this.service = service;
    }

    @GetMapping("/{username}")
    public UserDto login(@PathVariable("username") String username) {
        this.logger.info("Received a GET request on /users/" + username);
        try {
            return this.service.login(username);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Username does not match authentication", e);
        }

    }

    @PostMapping("/:register")
    public UserDto register(@RequestBody UserDto user) {
        this.logger.info("Received a POST request on /users/:register");
        return this.service.register(user);
    }

}
