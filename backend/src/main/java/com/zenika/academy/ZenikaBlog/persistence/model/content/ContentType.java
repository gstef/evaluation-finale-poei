package com.zenika.academy.ZenikaBlog.persistence.model.content;

public enum ContentType {
    TEXT,
    IMAGE
}
