package com.zenika.academy.ZenikaBlog.service;

import com.zenika.academy.ZenikaBlog.persistence.dao.ArticleRepository;
import com.zenika.academy.ZenikaBlog.persistence.model.analytics.Analytics;
import com.zenika.academy.ZenikaBlog.persistence.model.article.Article;
import com.zenika.academy.ZenikaBlog.persistence.model.article.Category;
import com.zenika.academy.ZenikaBlog.persistence.model.tags.Tags;
import com.zenika.academy.ZenikaBlog.web.dto.AnalyticsDto;
import com.zenika.academy.ZenikaBlog.web.dto.DataDto;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AnalyticsService {
    private Logger logger;
    private ArticleRepository articleRepository;

    @Autowired
    public AnalyticsService(Logger logger, ArticleRepository articleRepository) {
        this.logger = logger;
        this.articleRepository = articleRepository;
    }

    public DataDto getData() {
        this.logger.info("Received a request for data !");
        return new DataDto(this.articleRepository.getAuthors(),
                Arrays.stream(Category.values())
                        .map(Enum::toString)
                        .collect(Collectors.toList()),
                new ArrayList<>(this.articleRepository.getTags()
                        .stream()
                        .map(Tags::get)
                        .flatMap(Collection::stream)
                        .collect(Collectors.toSet())));
    }

    public AnalyticsDto getAnalytics(String author, Category category, String tag) {
        this.logger.info("Received a request for stats !");
        List<Article> articles = this.articleRepository.findAll();
        if (author != null) {
            articles = articles.stream()
                    .filter(a -> a.getUser().getUsername().equals(author))
                    .collect(Collectors.toList());
        }
        if (category != null) {
            articles = articles.stream()
                    .filter(a -> a.getCategory().equals(category))
                    .collect(Collectors.toList());
        }
        if (tag != null) {
            articles = articles.stream()
                    .filter(a -> a.getTags().contains(tag))
                    .collect(Collectors.toList());
        }
        return AnalyticsDto.mapper(new Analytics(articles));
    }

}
