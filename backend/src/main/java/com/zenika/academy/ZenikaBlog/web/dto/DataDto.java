package com.zenika.academy.ZenikaBlog.web.dto;

import lombok.Value;

import java.util.List;

@Value
public class DataDto {
    List<String> authors;
    List<String> categories;
    List<String> tags;
}
