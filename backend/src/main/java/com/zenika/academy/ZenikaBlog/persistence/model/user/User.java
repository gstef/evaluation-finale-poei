package com.zenika.academy.ZenikaBlog.persistence.model.user;

import com.zenika.academy.ZenikaBlog.persistence.model.article.Article;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "users")
public class User {
    @Id
    private UUID id;
    private String username;
    private String email;
    private String password;

    @OneToMany(mappedBy = "user")
    private List<Article> articles;

    @Deprecated
    public User() {
    }

    public User(UUID id, String username, String email, String password) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.articles = new ArrayList<>();
    }

    public UUID getId() {
        return this.id;
    }

    public String getUsername() {
        return this.username;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPassword() {
        return this.password;
    }

    public List<Article> getArticles() {
        return this.articles;
    }
}
