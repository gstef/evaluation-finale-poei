package com.zenika.academy.ZenikaBlog.web.dto;

import com.zenika.academy.ZenikaBlog.persistence.model.analytics.Analytics;
import lombok.Value;

@Value
public class AnalyticsDto {
    int articleNumber;
    float averageTagNumberPerArticle;
    float averageWordNumberPerArticle;

    public static AnalyticsDto mapper(Analytics analytics) {
        return new AnalyticsDto(analytics.getArticleNumber(), analytics.getAverageTagNumberPerArticle(),
                analytics.getAverageWordNumberPerArticle());
    }
}
