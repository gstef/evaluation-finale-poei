#!/bin/bash

docker-compose down

docker container rm zblog-postgres -f
docker container rm zblog-backend -f
docker container rm zblog-frontend -f

set -e

cd backend

mvn clean package

docker build -t zblog-backend .

cd ../frontend

npm install
npm run ng build -- --prod --output-path=dist

docker build -t zblog-frontend .

cd ..

docker-compose up