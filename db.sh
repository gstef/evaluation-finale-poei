#!/bin/bash

docker container rm -f zblog-postgres

docker container run --name zblog-postgres -p5432:5432 -e POSTGRES_PASSWORD=mysecretpassword -d postgres:12